function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your surname?');
    const birthday = prompt('Enter date of your birth (dd.mm.yyyy)');

    return newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        getAge() {
            let now = new Date()
            let birthdayNew= birthday.split(".");
            let date = new Date(birthdayNew[2]+ "-" + birthdayNew[1] + "-" + birthdayNew[0]);
            return Math.floor((now-date)/31536000000);

        },
        getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6)
        },
    }
}

console.log(createNewUser());
console.log(newUser.getAge());
console.log(newUser.getPassword());
