const getFirstNumber = +prompt("Enter first number");
const getSecondNumber = +prompt("Enter second number");
const getSign = prompt("Enter your operation");

function calc(num1, num2, sign) {
    switch (sign) {
        case '+':
           return num1 + num2;
        case '-':
           return  num1 - num2;
        case '/':
            return num1 / num2;
        case '*':
            return num1 * num2;
    }
}

console.log(calc(getFirstNumber, getSecondNumber, getSign));

